require 'rmp_sql_ext'
require_dependency 'a_common_libs/field_format'
require_dependency 'redmine/acl_date_time_format'

Redmine::Plugin.register :a_common_libs do
  name 'A common libraries'
  author 'Danil Kukhlevskiy'
  description 'This is a plugin for including common libraries'
  version '2.1.10'
  url 'http://rmplus.pro/'
  author_url 'http://rmplus.pro/'

  settings partial: 'settings/a_common_libs',
           default: { autoconfig_libs: true },
           auto: {}

  menu :custom_menu, :us_favourite_proj_name, nil, caption: Proc.new{ ('<div class="title">' + User.current.favourite_project.name + '</div>').html_safe }, if: Proc.new { User.current.logged? && User.current.favourite_project.is_a?(Project) }
  menu :custom_menu, :us_favourite_proj_issues, nil, caption: Proc.new{ ('<a href="' + Redmine::Utils.relative_url_root + '/projects/'+User.current.favourite_project.identifier+'/issues" class="no_line"><span>' + I18n.t(:label_issue_plural) + '</span></a>').html_safe }, if: Proc.new { User.current.logged? && User.current.favourite_project.is_a?(Project) }
  menu :custom_menu, :us_favourite_proj_new_issue, nil, caption: Proc.new{ ('<a href="' + Redmine::Utils.relative_url_root + '/projects/'+User.current.favourite_project.identifier+'/issues/new" class="no_line"><span>' + I18n.t(:label_issue_new) + '</span></a>').html_safe}, if: Proc.new { User.current.logged? && User.current.favourite_project.is_a?(Project) }
  menu :custom_menu, :us_favourite_proj_wiki, nil, caption: Proc.new{ ('<a href="' + Redmine::Utils.relative_url_root + '/projects/'+User.current.favourite_project.identifier+'/wiki" class="no_line"><span>' + I18n.t(:label_wiki) + '</span></a>').html_safe }, if: Proc.new { User.current.logged? && User.current.favourite_project.is_a?(Project) && User.current.favourite_project.module_enabled?(:wiki) }
  menu :custom_menu, :us_favourite_proj_dmsf, nil, caption: Proc.new{ ('<a href="' + Redmine::Utils.relative_url_root + '/projects/'+User.current.favourite_project.identifier+'/dmsf" class="no_line"><span>' + I18n.t(:label_dmsf) + '</span></a>').html_safe }, if: Proc.new { User.current.logged? && User.current.favourite_project.is_a?(Project) && User.current.favourite_project.module_enabled?(:dmsf) && Redmine::Plugin.installed?(:redmine_dmsf) }
  menu :custom_menu, :us_new_issue, nil, caption: Proc.new{ ('<a href="' + Redmine::Utils.relative_url_root + '/projects/'+User.current.favourite_project.identifier+'/issues/new" class="no_line"><span>' + I18n.t(:us_of_issue) + '</span></a>').html_safe }, if: Proc.new { User.current.logged? && User.current.favourite_project.is_a?(Project) }
end

Rails.application.config.to_prepare do
  ApplicationController.send(:include, ACommonLibs::ApplicationControllerPatch)
  User.send(:include, ACommonLibs::UserPatch)
  Query.send(:include, ACommonLibs::QueryPatch)
  if Redmine::VERSION.to_s >= '3.0.0'
    Issue.send(:include, ACommonLibs::IssuePatch)
  end
  SettingsController.send(:include, ACommonLibs::SettingsControllerPatch)
  ActionView::Base.send(:include, ACommonLibsHelper)
  ApplicationController.send(:include, ACommonLibsHelper)
  ApplicationHelper.send(:include, ACommonLibs::ApplicationHelperPatch)
  CustomFieldValue.send :include, ACommonLibs::CustomFieldValuePatch
  Redmine::Acts::Customizable::InstanceMethods.send :include, ACommonLibs::ActAsCustomizablePatch
  CustomFieldsController.send :include, ACommonLibs::CustomFieldsControllerPatch

  begin
    ACommonLibs::CssBtnIconsUtil.generate_css_file
  rescue Exception => ex
    Rails.logger.info "WARNING: Cannot generate custom css for button icons #{ex.message}" if Rails.logger
  end

end

Rails.application.config.after_initialize do
  Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['autoconfig_libs'] = true

  # Select2
  if Redmine::Plugin.installed?(:luxury_buttons) ||
     Redmine::Plugin.installed?(:kpi) ||
     Redmine::Plugin.installed?(:ldap_users_sync) ||
     Redmine::Plugin.installed?(:magic_my_page) ||
     Redmine::Plugin.installed?(:clear_plan) ||
     Redmine::Plugin.installed?(:goals) ||
     Redmine::Plugin.installed?(:extra_queries) ||
     Redmine::Plugin.installed?(:usability) ||
     Redmine::Plugin.installed?(:under_construction) ||
     Redmine::Plugin.installed?(:rmplus_devtools) ||
     Redmine::Plugin.installed?(:service_desk) ||
     Redmine::Plugin.installed?(:training) ||
     Redmine::Plugin.installed?(:shipping) ||
     Redmine::Plugin.installed?(:custom_menu) ||
     Redmine::Plugin.installed?(:rmplus_tags) ||
     Redmine::Plugin.installed?(:rm_custom_pdf)

    Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_select2_lib'] = true
  end

  # Highcharts
  if Redmine::Plugin.installed?(:kpi) ||
     Redmine::Plugin.installed?(:clear_plan) ||
     Redmine::Plugin.installed?(:service_desk)

    Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_jqplot_lib'] = true
  end

  # Twitter Bootstrap
  if Redmine::Plugin.installed?(:kpi) ||
     Redmine::Plugin.installed?(:ldap_users_sync) ||
     Redmine::Plugin.installed?(:magic_my_page) ||
     Redmine::Plugin.installed?(:clear_plan) ||
     Redmine::Plugin.installed?(:goals) ||
     Redmine::Plugin.installed?(:rm_user_mentions) ||
     Redmine::Plugin.installed?(:extra_queries) ||
     Redmine::Plugin.installed?(:usability) ||
     Redmine::Plugin.installed?(:under_construction) ||
     Redmine::Plugin.installed?(:rmplus_devtools) ||
     Redmine::Plugin.installed?(:service_desk)

    Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_bootstrap_lib'] = true
  end

  # Twitter Bootstrap for Luxury Buttons
  if Redmine::Plugin.installed?(:luxury_buttons)
    unless Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_bootstrap_lib']
      Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_bootstrap_lib_for_luxury_buttons'] = true
    end
  end

  # QuarterMonthPicker and moment.js
  if Redmine::Plugin.installed?(:goals)
    Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_qmpicker'] = true
  end

  # Javascript Patch
  if Redmine::Plugin.installed?(:luxury_buttons) ||
     Redmine::Plugin.installed?(:kpi) ||
     Redmine::Plugin.installed?(:ldap_users_sync) ||
     Redmine::Plugin.installed?(:magic_my_page) ||
     Redmine::Plugin.installed?(:clear_plan) ||
     Redmine::Plugin.installed?(:goals) ||
     Redmine::Plugin.installed?(:rm_user_mentions) ||
     Redmine::Plugin.installed?(:service_desk) ||
     Redmine::Plugin.installed?(:training) ||
     Redmine::Plugin.installed?(:rmplus_tags)

    Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_javascript_patches'] = true
  end

  # Modal windows
  if Redmine::Plugin.installed?(:luxury_buttons) ||
     Redmine::Plugin.installed?(:kpi) ||
     Redmine::Plugin.installed?(:ldap_users_sync) ||
     Redmine::Plugin.installed?(:magic_my_page) ||
     Redmine::Plugin.installed?(:clear_plan) ||
     Redmine::Plugin.installed?(:goals) ||
     Redmine::Plugin.installed?(:extra_queries) ||
     Redmine::Plugin.installed?(:rm_user_mentions) ||
     Redmine::Plugin.installed?(:service_desk) ||
     Redmine::Plugin.installed?(:training) ||
     Redmine::Plugin.installed?(:shipping) ||
     Redmine::Plugin.installed?(:custom_menu) ||
     Redmine::Plugin.installed?(:rmplus_tags)

    Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_modal_windows'] = true
  end

  # FontAwasome
  if Redmine::Plugin.installed?(:ldap_users_sync) ||
     Redmine::Plugin.installed?(:goals) ||
     Redmine::Plugin.installed?(:magic_my_page) ||
     Redmine::Plugin.installed?(:luxury_buttons)
    Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_font_awesome'] = true
  end

  #Period Picker
  # if Redmine::Plugin.installed?(:luxury_buttons)
  #   Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]['enable_periodpicker'] = true
  # end

  if Setting.plugin_a_common_libs.blank? || Setting.plugin_a_common_libs[:autoconfig_libs].present? || Setting.plugin_a_common_libs['autoconfig_libs'].present?
    Setting.plugin_a_common_libs = Redmine::Plugin::registered_plugins[:a_common_libs].settings[:auto]
  end

end
require 'a_common_libs/view_hooks'
