module ACommonLibs
  module ApplicationHelperPatch
    def self.included(base)
      base.send(:include, InstanceMethods)

      base.class_eval do
        alias_method_chain :calendar_for, :acl
      end
    end

    module InstanceMethods

      def calendar_for_with_acl(field_id)

        if Setting.plugin_a_common_libs['enable_periodpicker']
          include_calendar_headers_tags
          javascript_tag("$(function() {
              $('##{field_id}').periodpicker(periodpickerOptions);
          });")
        else
          calendar_for_without_acl(field_id)
        end
      end

    end

  end
end
