module ACommonLibs
  module QueryPatch
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do

        alias_method_chain :sql_for_field, :acl
        alias_method_chain :date_clause, :acl

        self.operators_by_filter_type[:acl_date_time] = %w(= >< >= <= !* *)
      end
    end

    module InstanceMethods

      def sql_for_field_with_acl(field, operator, value, db_table, db_field, is_custom_filter=false)
        if operator == '><' && type_for(field) == :acl_date_time
          sql = date_clause(db_table, db_field, Time.parse(value[0]), Time.parse(value[1]), is_custom_filter)
        elsif operator == '<=' && type_for(field) == :acl_date_time
          sql = date_clause(db_table, db_field, nil, Time.parse(value.first), is_custom_filter)
        elsif operator == '>=' && type_for(field) == :acl_date_time
          sql = date_clause(db_table, db_field, Time.parse(value.first), nil, is_custom_filter)
        else
          sql = sql_for_field_without_acl(field, operator, value, db_table, db_field, is_custom_filter)
        end
        sql
      end

      def date_clause_with_acl(table, field, from, to, is_custom_filter=false)
        if (from && from.is_a?(Time)) || (to && to.is_a?(Time))
          s = []

          if from
            from = User.current.acl_user_to_server_time(from, true)
            from = from - 1

            if Redmine::VERSION.to_s >= '3.0.0'
              s << ("#{table}.#{field} > '%s'" % [quoted_time(from, is_custom_filter)])
            else
              s << ("#{table}.#{field} > '%s'" % from.strftime("%Y-%m-%d %H:%M:%S"))
            end
          end
          if to
            to = User.current.acl_user_to_server_time(to, true)
            if Redmine::VERSION.to_s >= '3.0.0'
              s << ("#{table}.#{field} <= '%s'" % [quoted_time(to, is_custom_filter)])
            else
              s << ("#{table}.#{field} <= '%s'" % to.strftime("%Y-%m-%d %H:%M:%S"))
            end
          end
          s.join(' AND ')
        else
          if Redmine::VERSION.to_s >= '3.0.0'
            date_clause_without_acl(table, field, from, to, is_custom_filter)
          else
            date_clause_without_acl(table, field, from, to)
          end
        end
      end
    end

  end
end